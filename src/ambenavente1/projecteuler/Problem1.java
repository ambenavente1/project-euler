package ambenavente1.projecteuler;

/**
 * Finds the sum of all the multiples of 3 or 5 below 1000.
 *
 * @author Anthony Benavente
 * @version 4/23/14
 */
public class Problem1 {

    public void go() {
        int sum = 0;
        for (int i = 1; i < 1000; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                sum += i;
            }
        }
        System.out.println(sum);
    }

    public static void main(String[] args) {
        new Problem1().go();
    }
}
