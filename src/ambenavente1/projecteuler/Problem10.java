package ambenavente1.projecteuler;

import java.util.HashSet;
import java.util.List;

/**
 * Find the sum of all the primes below two million.
 *
 * @author Anthony Benavente
 * @version 5/1/14
 */
public class Problem10 {

    public void go() {
        Timer timer = new Timer();
        long sum = 0;
        timer.start();
        for (Integer i : sieveOfEratosthenes(2000000)) {
            sum += i;
        }
        timer.stop();

        System.out.println(sum);
        System.out.println(timer);
    }

    private HashSet<Integer> sieveOfEratosthenes(int max) {
        HashSet<Integer> nums = new HashSet<Integer>();
        for (int i = 2; i < max; i++) {
            nums.add(i);
        }

        for (int i = 2; i <= Math.sqrt(max); i++) {
            for (int j = i; i*j < max; j++) {
                if (j*i != 2) {
                    nums.remove(j*i);
                }
            }
        }

        return nums;
    }

    public static void main(String[] args) {
        new Problem10().go();
    }
}
