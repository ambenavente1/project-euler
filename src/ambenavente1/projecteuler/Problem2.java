package ambenavente1.projecteuler;

/**
 * By considering the terms in the Fibonacci sequence whose values do not
 * exceed four million, finds the sum of the even-valued terms.
 *
 * @author Anthony Benavente
 * @version 4/23/14
 */
public class Problem2 {

    public void go() {
        int sum = 0;
        for (int a = 1,
                 b = 1,
                 c = a + b;
             c < 4000000;
                 a = b,
                 b = c,
                 c = a + b) {
            if (c  % 2 == 0) {
                sum += c;
            }
        }
        System.out.println(sum);
    }

    public static void main(String[] args) {
        new Problem2().go();
    }
}
