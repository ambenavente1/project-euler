package ambenavente1.projecteuler;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @version 4/23/14
 */
public class Problem23 {

    public void go() {
        HashSet<Integer> all = new HashSet<Integer>();
        for (int i = 1; i < 28123; i++) {
            all.add(i);
        }

        List<Integer> abundants = new LinkedList<Integer>();
        for (int i = 1; i < 28123; i++) {
            if (isAbundant(i)) abundants.add(i);
        }

        for (Integer i : abundants) {
            for (Integer j : abundants) {
                if (i + j < 28123) {
                    all.remove(i+j);
                }
            }
        }

        long sum = 0;
        for (Integer i : all) {
            sum += i;
        }

        System.out.println(sum);
    }

    private boolean isAbundant(int num) {
        List<Integer> divisors = getDivisors(num);
        long sum = 0;
        for (Integer i : divisors) {
            sum += i;
        }
        return sum > num;
    }

    private boolean isPerfect(int num) {
        List<Integer> divisors = getDivisors(num);
        long sum = 0;
        for (Integer i : divisors) {
            sum += i;
        }
        return sum == num;
    }

    private List<Integer> getDivisors(int num) {
        List<Integer> divisors = new LinkedList<Integer>();
        for (int i = 1; i <= (int)(Math.sqrt(num)); i++) {
            if (num % i == 0) {
                if (!divisors.contains(i)) {
                    divisors.add(i);
                    if (num / i != num) {
                        if (!divisors.contains(num / i)) {
                            divisors.add(num / i);
                        }
                    }
                }
            }
        }
        return divisors;
    }


    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        new Problem23().go();
        long end = System.currentTimeMillis();
        System.out.println("Time took " + (end - start) + " ms");
    }
}
