package ambenavente1.projecteuler;

import java.util.ArrayList;
import java.util.List;

/**
 * Finds the largest prime factor of the number 600851475143.
 *
 * @author Anthony Benavente
 * @version 4/23/14
 */
public class Problem3 {

    public void go() {
        long num = 600851475143L;
        long largestPrime = Integer.MIN_VALUE;
        for (Integer i: getDivisors(num)) {
            if (isPrime(i)) {
                if (i > largestPrime) largestPrime = i;
            }
        }
        System.out.println(largestPrime);
    }

    private List<Integer> getDivisors(long num) {
        List<Integer> nums = new ArrayList<Integer>();
        for (int i = 1; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                nums.add(i);
                if (num / i != num) {
                    nums.add((int)(num / i));
                }
            }
        }
        return nums;
    }

    private boolean isPrime(int num) {
        boolean isPrime = true;
        for (int i = 2; isPrime && i <= Math.sqrt(num); i++) {
            isPrime = num % i != 0;
        }
        return isPrime;
    }

    public static void main(String[] args) {
        new Problem3().go();
    }
}
