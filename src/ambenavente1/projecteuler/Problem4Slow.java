package ambenavente1.projecteuler;

import java.util.ArrayList;
import java.util.List;

/**
 * Finds the largest palindrome made from the product of two 3-digit numbers.
 *
 * @author Anthony Benavente
 * @version 4/23/14
 */
public class Problem4Slow {

    public void go() {

        final int digits = 5;

        // Default method
        long start = System.currentTimeMillis();
        long largestPal = 0;
        long startNum = (long) Math.pow(10, digits - 1);
        long endNum = (long) Math.pow(10, digits);
        for (long i = startNum; i < endNum; i++) {
            for (long j = startNum; j < endNum; j++) {
                long num = i * j;
                if (isPalindrome(num)) {
                    largestPal = Math.max(num, largestPal);
                }
            }
        }
        long end = System.currentTimeMillis();
        System.out.println(largestPal);
        System.out.println("Time took: " + (end - start) + "ms");
    }

    private boolean isPalindrome(long num) {
        String numStr = String.valueOf(num);

        boolean pal = true;
        for (int i = 0; pal && i < numStr.length() / 2; i++) {
            pal = numStr.charAt(i) == numStr.charAt(numStr.length() - i - 1);
        }
        return pal;
    }

    /**
     * Checks if a number is a palindrome by converting it to a string. A
     * palindromic number is a number that reads the same forwards and
     * backwards.
     *
     * @param num the number to check for
     * @return if the number is a palindrome
     */
    private boolean isPalindrome(int num) {
        return isPalindrome((long)num);
    }

    public static void main(String[] args) {
        new Problem4Slow().go();
    }
}
