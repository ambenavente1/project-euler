package ambenavente1.projecteuler;

import java.util.ArrayList;
import java.util.List;

/**
 * What is the smallest positive number that is evenly divisible by all of the
 * numbers from 1 to 20?
 *
 * @author Anthony Benavente
 * @version 4/28/14
 */
public class Problem5 {

    public void go() {
        List<Integer> nums = new ArrayList<Integer>();
        for (int i = 1; i <= 20; i++) nums.add(i);
        System.out.println(lcm(nums));
    }

    private int lcm(List<Integer> nums) {
        int lcm = nums.get(0);
        for (int i = 1; i < nums.size(); i++) {
            lcm = lcm(lcm, nums.get(i));
        }
        return lcm;
    }

    private int lcm(int a, int b) {
        return  a * (b / gcd(a, b));
    }

    private int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    public static void main(String[] args) {
        new Problem5().go();
    }
}
