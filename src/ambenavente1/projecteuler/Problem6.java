package ambenavente1.projecteuler;

/**
 * Find the difference between the sum of the squares of the first one hundred
 * natural numbers and the square of the sum.
 *
 * @author Anthony Benavente
 * @version 4/28/14
 */
public class Problem6 {

    public void go() {
        System.out.println(squareOfSum(100) - sumOfSquares(100));
    }

    private long sumOfSquares(int n) {
        long sumOfSquares = 0;
        for (int i = 1; i <= n; i++) {
            sumOfSquares += (i * i);
        }
        return sumOfSquares;
    }

    private long squareOfSum(int n) {
        return (long) Math.pow(n * (n + 1) / 2, 2);
    }

    public static void main(String[] args) {
        new Problem6().go();
    }
}
