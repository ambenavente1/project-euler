package ambenavente1.projecteuler;

/**
 * What is the 10,001st prime number?
 *
 * @author Anthony Benavente
 * @version 4/28/14
 */
public class Problem7 {

    public void go() {
        Timer timer = new Timer();
        timer.start();
        long nthPrime = getNthPrime(10001);
        timer.stop();
        System.out.println(nthPrime);
        System.out.println(timer);
    }

    private long getNthPrime(int n) {
        int accumulator = 0;
        int i = 0;
        for (i = 2; accumulator != n; i++) {
            if (isPrime(i)) accumulator++;
        }
        return i - 1;
    }

    private boolean isPrime(int num) {
        boolean isPrime = true;
        for (int i = 2; isPrime && i <= Math.sqrt(num); i++) {
            isPrime = num % i != 0;
        }
        return isPrime;
    }

    public static void main(String[] args) {
        new Problem7().go();
    }
}
