package ambenavente1.projecteuler;

/**
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000
 * and a < b < c. Find the product a, b, and c.
 *
 * @author Anthony Benavente
 * @version 5/1/14
 */
public class Problem9 {

    public void go() {
        Timer timer = new Timer();
        int prod = -1;

        timer.start();
        for (int a = 1; prod == -1 && a < 1000; a++) {
            for (int b = a; prod == -1 && b < 1000; b++) {
                if (a == 200) {
                    System.out.print("");
                }
                double c = Math.sqrt(a * a + b * b);
                if (isWhole(c)) {
                    if (a + b + c == 1000) {
                        prod = (int) (a * b * c);
                    }
                }
            }
        }
        timer.stop();

        System.out.println(prod);
        System.out.println(timer);
    }

    private boolean isWhole(double num) {
        return num == Math.round(num);
    }

    public static void main(String[] args) {
        new Problem9().go();
    }
}
