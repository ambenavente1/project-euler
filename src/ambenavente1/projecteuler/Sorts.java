package ambenavente1.projecteuler;

import java.util.Arrays;
import java.util.Random;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @version 4/28/14
 */
public class Sorts {
    public static final Sorts INSTANCE = new Sorts();

    public <T extends Comparable<? super T>> void shellSort(T[] a) {
        for (int n = a.length / 2; n > 0; n /= 2) {

            // The algorithm is more efficient when dealing with odd "n"s
            if (n % 2 == 0) {
                n++;
            }

            for (int i = 0; i < n; i++) {
                spacedInsertionSort(a, i, a.length, n);
            }
        }

//        displayArray(a);
    }

    public <T extends Comparable<? super T>>
        void spacedInsertionSort(T[] a,
                                 int start,
                                 int end,
                                 int modifier) {
        for (int i = start; i < end; i += modifier) {
            for (int j = i; j >= modifier && less(a[j],
                    a[j - modifier]); j -= modifier) {
                swap(a, j, j - modifier);
            }
        }
    }

    public <T extends Comparable<? super T>> void insertionSort(T[] a) {
        spacedInsertionSort(a, 1, a.length, 1);
    }

    public <T extends Comparable<? super T>> void selectionSort(T[] a) {
        for (int i = 0; i < a.length; i++) {
            int minIndex = i;
            for (int j = i; j < a.length; j++) {
                if (a[j].compareTo(a[minIndex]) < 0) {
                    minIndex = j;
                }
            }
            swap(a, minIndex, i);
        }
    }

    public <T extends Comparable<? super T>> void bubbleSort(T[] array) {
    }


    private <T extends Comparable<? super T>> void swap(T[] a,
                                                        int index1,
                                                        int index2) {
        T tmp = a[index1];
        a[index1] = a[index2];
        a[index2] = tmp;
    }

    private boolean less(Comparable a,
                         Comparable b) {
        return a.compareTo(b) < 0;
    }

    private <T extends Comparable<? super T>> void displayArray(T[] array) {
        System.out.println(Arrays.toString(array));
    }

    public static void main(String[] args) {
        Integer[] a = getRandArray(100000, 100000);
        Integer[] b = a.clone();

        long start = System.currentTimeMillis();
        Sorts.INSTANCE.insertionSort(b);
        long end = System.currentTimeMillis();
        printTime(start, end, "insertion");

        b = a.clone();
        start = System.currentTimeMillis();
        Sorts.INSTANCE.selectionSort(b);
        end = System.currentTimeMillis();
        printTime(start, end, "selection");

        b = a.clone();
        start = System.currentTimeMillis();
        Sorts.INSTANCE.shellSort(b);
        end = System.currentTimeMillis();
        printTime(start, end, "shell");

        b = a.clone();
        start = System.currentTimeMillis();
        Arrays.sort(b);
        end = System.currentTimeMillis();
        printTime(start, end, "Java's sort method");
    }

    private static Integer[] getRandArray(int n, int max) {
        Random rand = new Random();
        Integer[] randArray = new Integer[n];
        for (int i = 0; i < n; i++) {
            randArray[i] = rand.nextInt(max);
        }
        return randArray;
    }

    private static void printTime(long start, long end, String event) {
        System.out.println("It took " + (end - start) + " ms for " + event);
    }
}
