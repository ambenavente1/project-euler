package ambenavente1.projecteuler;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @version 4/28/14
 */
public class Timer {

    private boolean started = false;
    private long startTime = -1;
    private long endTime = -1;

    public void start() {
        if (started) {
            System.out.println("Started has already been called...");
        } else {
            started = true;
            startTime = System.currentTimeMillis();
        }
    }

    public void stop() {
        if (!started) {
            System.out.println("Timer hasn't been started...");
        } else {
            endTime = System.currentTimeMillis();
        }
    }

    public long getElapsed() {
        return endTime - startTime;
    }

    public void reset() {
        started = false;
        startTime = -1;
        endTime = -1;
    }

    @Override
    public String toString() {
        return "The action took " + getElapsed() + " ms to complete...";
    }
}
